global.$ = {
	gulp: require("gulp"),
	gp: require("gulp-load-plugins")(),
	bs: require("browser-sync").create(),

	prefix: require("autoprefixer"),
	mqpacker: require("css-mqpacker"),
	sortMedia: require("sort-css-media-queries"),

	pngQuant: require("imagemin-pngquant"),
	jpegRecompress: require("imagemin-jpeg-recompress"),
	imageminSvgo: require("imagemin-svgo"),
	del: require("del"),

	npmfiles: require('npmfiles'),
	
	path: {
		tasks: require("./gulp/config.js")
	},
	minificate: false,
	minificateImg: false,
	TunnelFtp: false
};


$.path.tasks.forEach(function(taskPath) {
	require(taskPath)();
});

$.gulp.task('pluginsList', function() {
	return console.log($.gp);
});
// $.gulp.task("default", $.gulp.series('clean', "html", "vendorCss", "styles", 'scripts', 'bundle', "favicons", "fonts", "images", 'svgSprite', "watch", "serve"), function(done){
// 	done();
// });
$.gulp.task('default', $.gulp.series('clean', $.gulp.parallel('html', 'favicons', 'fonts', 'styles', 'vendorCss', 'scripts', 'bundle', 'images', 'svgSprite'), $.gulp.parallel('watch', 'serve')), function(done) {
	done();
});


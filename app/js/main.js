(function(){
	var doc = $(document),
		win = $(window);

	var waitForFinalEvent = (function () {
		var timers = {};
		return function (callback, ms, uniqueId) {
			if (!uniqueId) {
				uniqueId = "Don't call this twice without a uniqueId";
			}
			if (timers[uniqueId]) {
				clearTimeout (timers[uniqueId]);
			}
			timers[uniqueId] = setTimeout(callback, ms);
		};
	})();

	doc.ready(function() {
		$('[data-toggle="tooltip"]').tooltip();
		$('[data-toggle="dropdown"]').dropdown({
			flip: false,
		});
		$('[data-toggle="dropdown-placement"]').dropdown();
		$('[data-dropdown-disable]').click(function() {
			// console.log($(this).closest('.dropdown').find('[data-toggle="dropdown-placement"]'))
			$(this).closest('.dropdown-menu').removeClass('open show')
		})
		$('.dropdown').on('show.bs.dropdown', function(e) {
			if ( $(this).closest('.invoice-wrap').length ) {
				$('.invoice-wrap').addClass('stub');
			}
			console.log($(e.target))
			if ( $(e.target).data('highlight') == 'tr' ) {
				console.log('lalala')
				$(e.target).closest('tr').addClass('background-grey')
			}
		});
		$('.dropdown').on('hide.bs.dropdown', function(e) {
			if ( $(this).closest('.invoice-wrap').length ) {
				$('.invoice-wrap').removeClass('stub');
			}
			if ( $(e.target).data('highlight') == 'tr' ) {
				$(e.target).closest('tr').removeClass('background-grey')
			}
		})
		$('.modal').on('hidden.bs.modal', function(e) {
			if ( $('.modal').hasClass('show') ) {
				$('body').addClass('modal-open');
				if ( $(window).width() > 767 ) {
					$('body').css({'padding-right': '17px'});
				}
			}
		});
		$(document).on('click', function (e) {
			if ( !$(e.target).closest('.dropdown').length 
				&& !$(e.target).closest('.datepicker--cell.datepicker--cell-day').length 
				&& !$(e.target).closest('.datepicker--nav-title').length 
				&& !$(e.target).closest('.datepicker--nav-action').length ) {
				$('.dropdown-menu').removeClass('visible show');
				$('.dropdown').removeClass('visible show');
			}
		});
		(function() {
			var count = 0;
			$('.dropdown-menu[aria-labelledby]').click(function(e) {
				if ( !$(e.target).closest('.dropdown-drop').length ) {
					$('.dropdown-drop-menu').removeClass('visible show');
					$('.dropdown-drop').removeClass('visible show');
				};
				if ( $(e.target).closest('.dropdown-drop').length ) {
					$(e.target).closest('.dropdown-drop').parent().siblings().find('.dropdown-drop').removeClass('visible show')
					$(e.target).closest('.dropdown-drop').parent().siblings().find('.dropdown-drop-menu').removeClass('visible show')
				}
				if ( $(e.target).data('check-item') == 'dropdown' ) {
					if ( $(e.target).is('.checkbox-input') ) {
						if ( $(e.target).prop('checked') == true ) {
							count += 1;
							if ( count > 0 ) {
								$(e.target).closest('.dropdown').addClass('itemsSelected');
							}
						} else {
							count -= 1;
							if ( count < 0 ) {
								$(e.target).closest('.dropdown').removeClass('itemsSelected');
							}
						}
						// e.preventDefault();
					}
					if ( $(e.target).is('.radio-input') ) {
						if ( $(e.target).prop('checked') == true ) {
							$(e.target).closest('.dropdown').addClass('itemsSelected');
						} else {
							$(e.target).closest('.dropdown').removeClass('itemsSelected');
						}
					}
					if ( $(e.target).is('a') ) {
						$(e.target).toggleClass('selected');
						if ( $(e.target).hasClass('selected') ) {
							count += 1;
							if ( count > 0 ) {
								$(e.target).closest('.dropdown').addClass('itemsSelected');
							}
						} else {
							count -= 1;
							if ( count < 1 ) {
								$(e.target).closest('.dropdown').removeClass('itemsSelected');
							}
						}
						e.preventDefault();
					}
				}
				if ( $(e.target).data('toggle') == 'modal' ) {
					$( $(e.target).data('target') ).modal()
				}
				// console.log($(this))
				if ( $(this).hasClass('keep-open') ) {
					console.log('lalalal')
					e.stopPropagation();
				}
			});
			$('[data-clear="dropdown"]').click(function() {
				var parent = $(this).closest('[data-check-items="dropdown"]');
				var items = parent.find('[data-check-item="dropdown"]');
				if ( items.is('.checkbox-input') || items.is('.radio-input') ) {
					items.prop('checked', false);
				}
				if ( items.is('a') ) {
					items.each(function() {
						$(this).removeClass('selected');
					})
				}
				count = 0;
				parent.removeClass('itemsSelected');
			})
		}())
		$('[data-step]').click(function() {
			var closestItem = $(this).closest('[data-step-item]');
			var closestItemID = closestItem.attr('id')
			$('[data-step-item]').removeClass('show active');
			if ( $(this).data('step') == 'prev' ) {
				var closestItemPrev = closestItem.prev();
				var prevID = closestItemPrev.attr('id');
				$('[data-step-preview][href="#'+prevID+'"]').trigger('click')
			}
			if ( $(this).data('step') == 'next' ) {
				var closestItemNext = closestItem.next();
				var nextID = closestItemNext.attr('id');
				$('[data-step-preview][href="#'+nextID+'"]').trigger('click');
				$('[data-step-preview][href="#'+closestItemID+'"]').addClass('checked')
			}
		});
		$('.radio-input').change(function() {
			if ( $(this).closest('[data-check="radio-Input"]').length ) {
				if ( $(this).prop('checked') == true ) {
					$('[data-check="radio-Input"] .form-control').attr('disabled', true);
					$(this).closest('[data-check="radio-Input"]').find('.form-control').attr('disabled', false);
				};
			};
		});

		svg4everybody({
			polyfill: true
		});
		var taskSize = 0;
		$('.date-picker').datepicker({
			showOtherMonths: false,
			autoClose: true,
			// inline: true,
			prevHtml:   '<svg class="svg-icon">'+
							'<use xlink:href="images/svg-sprite/svgSprite.svg#shevron"></use>'+
						'</svg>',
			nextHtml:   '<svg class="svg-icon">'+
							'<use xlink:href="images/svg-sprite/svgSprite.svg#shevron"></use>'+
						'</svg>',
			onShow: function(dp, animationCompleted) {
				if ( !animationCompleted ) {
					dp.el.parentElement.appendChild(dp.$datepicker[0]);
					dp.el.parentElement.querySelector('.datepicker').classList.add('visible');
					dp.el.parentElement.querySelector('.svg-icon').classList.add('red');
				};
				if ( $(dp.el).closest('.popup-dropdown_calendar').length ) {
					$(dp.el).addClass('date-picker_double');
					$('.date-picker:not(.date-picker_double)').datepicker().data('datepicker').show();
				}
			},
			onHide: function(dp, animationCompleted) {
				if ( !animationCompleted ) {
					dp.el.parentElement.appendChild(dp.$datepicker[0]);
					dp.el.parentElement.querySelector('.datepicker').classList.remove('visible');
					dp.el.parentElement.querySelector('.svg-icon').classList.remove('red');
				};
				if ( $(dp.el).closest('.popup-dropdown_calendar').length ) {
					$(dp.el).removeClass('date-picker_double');
				}
			},
		})
		$('.date-picker-calendar').datepicker({
			showOtherMonths: false,
			autoClose: false,
			inline: true,
			prevHtml:   '<svg class="svg-icon">'+
							'<use xlink:href="images/svg-sprite/svgSprite.svg#shevron"></use>'+
						'</svg>',
			nextHtml:   '<svg class="svg-icon">'+
							'<use xlink:href="images/svg-sprite/svgSprite.svg#shevron"></use>'+
						'</svg>',
		});
		$('.date-picker-calendar').click(function() {
			$(this).closest('.popup-dropdown_calendar').find('.datepicker-inline').addClass('visible')
		});
		$('[data-calendar-clear]').click(function() {
			var datepickerInputs = $(this).closest('.popup-dropdown_calendar').find('.date-picker-calendar');
			var datepickers = $(this).closest('.popup-dropdown_calendar').find('.datepicker-inline');
			datepickerInputs.each(function(elem) {
				$(this).datepicker().data('datepicker').clear();
			});
			datepickers.removeClass('visible')
		})
		$('.date-picker-range').each(function() {
			$(this).dateRangePicker({
				autoClose: true,
				language: 'ru',
				separator: ' - ',
				showShortcuts: false,
				format: 'DD.MM.YYYY',
				customArrowPrevSymbol: '<svg class="svg-icon">'+
											'<use xlink:href="images/svg-sprite/svgSprite.svg#shevron"></use>'+
										'</svg>',
				customArrowNextSymbol: '<svg class="svg-icon">'+
											'<use xlink:href="images/svg-sprite/svgSprite.svg#shevron"></use>'+
										'</svg>',
				container: $(this).parent()
			});
		}).bind('datepicker-open', function(){
			if ( $(this).parent().hasClass('date-picker-box') ) {
				$(this).siblings('.svg-icon').addClass('white');
				$(this).addClass('dark');
			} else {
				$(this).siblings('.svg-icon').addClass('red');
			}
		}).bind('datepicker-change', function(event, obj){
			$(this).siblings('.svg-icon').addClass('position-applyed');
		}).bind('datepicker-close', function(){
			if ( $(this).parent().hasClass('date-picker-box') ) {
				$(this).siblings('.svg-icon').removeClass('white');
				$(this).removeClass('dark');
			} else {
				$(this).siblings('.svg-icon').removeClass('red');
			}
		});
		(function() {
			var flag = false;
			$('.input-toggle').click(function(evt) {
				evt.stopPropagation();
				if ( $(this).siblings('input').hasClass('date-picker') ) {
					$(this).siblings('input').datepicker().data('datepicker').show();
				} else {
					$(this).siblings('input').trigger('click');
				}
			});
		}());
		$('.custom-scroll').mCustomScrollbar({
			axis:"y",
			scrollInertia: 300
		});
		$('.custom-scroll-table').mCustomScrollbar({
			horizontalScroll: true,
			axis:"x",
			scrollInertia: 300,
			advanced:{
				autoExpandHorizontalScroll: true,
				updateOnContentResize: true,
				updateOnImageLoad: false
			},
			mouseWheel:{ enable: false },
		});
		$('[data-toggle="toggleVisible"]').click(function(e) {
			e.preventDefault();
			var target = $(this).data('target');
			$('[data-id="'+target+'"]').toggleClass('visible show');
			if ( $(this).closest('.dropdown').length ) {
				$(this).closest('.invoice-wrap').removeClass('stub');
			}
		});
		(function() {
			var count = 0;
			$('.table-count-list input[type="checkbox"]').on('change', function() {
				if ( $(this).prop('checked') == true ) {
					count += 1;
					if ( count > 0 ) {
						$('.check-condition').addClass('visible check-true');
					}
				} else {
					count -= 1;
					if ( count < 1 ) {
						$('.check-condition').removeClass('visible check-true');
					}
				}
			})
		}());

		$('[data-collapse-trigger]').click(function() {
			var target = $(this).data('target');
			$(this).toggleClass('active');
			$('[data-id="'+target+'"][data-collapse-cell]').toggleClass('d-none');
			$('[data-id="'+target+'"][data-collapse-cell-total]').toggleClass('d-none');
			if ( $(this).hasClass('active') ) {
				$('[data-id="'+target+'"][data-collapse-cell-title]').attr('colspan', '3');
			} else {
				$('[data-id="'+target+'"][data-collapse-cell-title]').attr('colspan', '1');
			}
			$(this).closest('.custom-scroll-table').mCustomScrollbar("update");
			// console.log('lalalal');
		});
		$('[data-collapse-row-trigger]').click(function() {
			var target = $(this).data('target');
			$(this).toggleClass('active');
			if ( $(this).hasClass('active') ) {
				$('[data-id="'+target+'"]').removeClass('d-none');
			} else {
				$('[data-id="'+target+'"]').find('[data-collapse-row-trigger]').removeClass('active');
				$('[data-id="'+ $('[data-id="'+target+'"]').find('[data-collapse-row-trigger]').data('target') +'"]').addClass('d-none');
				$('[data-id="'+target+'"]').addClass('d-none');
			}
			if ( $('[data-collapse-row-trigger].active').length <= 0 ) {
				$('[data-collapse-all-trigger]').removeClass('active')
			} else {
				$('[data-collapse-all-trigger]').addClass('active')
			}
		});
		$('[data-collapse-all-trigger]').click(function() {
			var _this = $(this);
			var row = $(this).closest('.table').find('tr[data-id]');
			_this.toggleClass('active');
			if ( _this.hasClass('active') ) {
				row.removeClass('d-none');
				$('.table tbody .table-collapse-btn').addClass('active');
				// row.find('.table-collapse-btn').addClass('active');
			} else {
				row.addClass('d-none')
				$('.table tbody .table-collapse-btn').removeClass('active');
			}
		});

		$('.nstSlider').nstSlider({
			"left_grip_selector": ".leftGrip",
			"value_changed_callback": function(cause, leftValue, rightValue) {
				$(this).parent().find('.leftGrip-value').text(leftValue);
			}
		});

		var header = $('.header');
		var footer = $('.footer');
		var btns = $('.wrap_btns');
		var sidebar = $('.sidebar');
		var headerH = header.outerHeight();
		var footerH = footer.outerHeight();
		var btnsH = btns.outerHeight();
		sidebar.css({'top': headerH + 20, 'bottom': footerH + 40});
		// $('.page-grid').css({'padding-top': headerH + 20});
		headerTop = header.offset().top;
		$(window).on('scroll', function() {
			var winTop = $(this).scrollTop();
			if ( winTop > headerTop ) {
				header.addClass('header_fixed');
			} else {
				header.removeClass('header_fixed');
			};
		});
		$(window).on('resize', function() {
			waitForFinalEvent(function() {
				header = $('.header');
				footer = $('.footer');
				headerH = header.outerHeight();
				footerH = footer.outerHeight();
				sidebar.css({'top': headerH + 20, 'bottom': footerH + 40});
				// $('.page-grid').css({'padding-top': headerH + 20});
			}, 500, 'resize');
		})
	});
}())
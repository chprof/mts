module.exports = function() {
	$.gulp.task("scripts", function() {
		return $.gulp.src("./app/js/main.js")
			.pipe($.gulp.dest("./build/js/"))
			.on("end", $.bs.reload);
	});
	$.gulp.task("bundle", function() {
		return $.gulp.src([
				"node_modules/svg4everybody/dist/svg4everybody.min.js",
				"node_modules/popper.js/dist/umd/popper.min.js",
				"node_modules/bootstrap/dist/js/bootstrap.min.js",
				"node_modules/moment/min/moment.min.js",
				"node_modules/jquery-date-range-picker/dist/jquery.daterangepicker.min.js",
				"node_modules/air-datepicker/dist/js/datepicker.min.js",
				"node_modules/jquery-mousewheel/jquery.mousewheel.js",
				"node_modules/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js",
				"node_modules/jquery-nstslider/dist/jquery.nstSlider.min.js"
				// "node_modules/dropzone/dist/min/dropzone.min.js"
			])
			.pipe($.gp.concat('vendor.min.js'))
			.pipe($.gulp.dest("./build/js/"))
			.on("end", $.bs.reload);
	});
};
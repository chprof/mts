module.exports = function() {
	$.gulp.task("html", function() {
		return $.gulp.src(["./app/*.html", "!./app/includes/*.html"])
			.pipe($.gp.rigger())
			.pipe($.gp.cached())
			.pipe($.gulp.dest("./build/"))
			.on("end", $.bs.reload);
	});
};